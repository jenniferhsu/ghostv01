/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.1

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_35B5BDD81778E2E7__
#define __JUCE_HEADER_35B5BDD81778E2E7__

//[Headers]     -- You can add your own extra header files here --
#include "JuceHeader.h"
#include "PluginProcessor.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Introjucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class Ghostv01AudioProcessorEditor  : public AudioProcessorEditor,
                                      public Timer,
                                      public SliderListener,
                                      public ButtonListener
{
public:
    //==============================================================================
    Ghostv01AudioProcessorEditor (Ghostv01AudioProcessor& ownerFilter);
    ~Ghostv01AudioProcessorEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void timerCallback();
    Ghostv01AudioProcessor* getProcessor() const
     {return static_cast <Ghostv01AudioProcessor*>(getAudioProcessor());}
    //[/UserMethods]

    void paint (Graphics& g);
    void resized();
    void sliderValueChanged (Slider* sliderThatWasMoved);
    void buttonClicked (Button* buttonThatWasClicked);

    // Binary resources:
    static const char* immortal_jellyfishedit_png;
    static const int immortal_jellyfishedit_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Slider> ghostLevelSld;
    ScopedPointer<Label> ghostLevelLbl;
    ScopedPointer<ToggleButton> removeNotesBtn;
    ScopedPointer<Slider> removeNotesSld;
    ScopedPointer<Label> removeNotesLbl;
    ScopedPointer<Slider> gn1AmountSld;
    ScopedPointer<Slider> gn2AmountSld;
    ScopedPointer<Slider> gn3AmountSld;
    ScopedPointer<Label> gn1Label;
    ScopedPointer<Label> gn2Label;
    ScopedPointer<Label> gn3Label;
    ScopedPointer<Label> removeNotesBtnLbl;
    Image cachedImage_immortal_jellyfishedit_png;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Ghostv01AudioProcessorEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_35B5BDD81778E2E7__
